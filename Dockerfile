FROM php:fpm

MAINTAINER Stefan Touhami
LABEL base PHP image for Symfony

RUN apt-get update && apt-get install -y \
    wget \
    unzip \
    openssl \
    sqlite3 \
    libzip-dev \
    libpq-dev \
    libcurl4-gnutls-dev \
    libicu-dev \
    libvpx-dev \
    libjpeg-dev \
    libpng-dev \
    libxpm-dev \
    zlib1g-dev \
    libfreetype6-dev \
    libxml2-dev \
    libexpat1-dev \
    libbz2-dev \
    libgmp3-dev \
    libldap2-dev \
    unixodbc-dev \
    libsqlite3-dev \
    libaspell-dev \
    libsnmp-dev \
    libpcre3-dev \
    libtidy-dev

ENV COMPOSER_ALLOW_SUPERUSER 1

RUN echo "$(curl -sS https://composer.github.io/installer.sig) -" > composer-setup.php.sig \
        && curl -sS https://getcomposer.org/installer | tee composer-setup.php | sha384sum -c composer-setup.php.sig \
        && php composer-setup.php && rm composer-setup.php* \
        && chmod +x composer.phar && mv composer.phar /usr/bin/composer \
        && composer --version

RUN docker-php-ext-install \
	pdo \
	pdo_mysql \
	gd \
	xml \
	zip \
	bz2 \
	opcache

RUN yes | pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini

CMD php-fpm
EXPOSE 9000
